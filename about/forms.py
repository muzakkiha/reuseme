from django import forms
from .models import Testimonial

# implement forms using ModelForm
class FormTestimoni(forms.Form):
    testimoni = forms.CharField(widget=forms.Textarea(attrs={
		'id' 			: 'post-text',
		'class'			: 'form-control',
		'required'		: True,
		'placeholder'	: 'Write your testimonial here.',
	}), label='', max_length=5000)
	
class Meta:
	model = Testimonial
	fields = ('testimoni',)
