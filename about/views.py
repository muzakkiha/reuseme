from django.shortcuts import render, render_to_response
from django.template import RequestContext, loader
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from accounts.models import Member

from .models import Testimonial
from .forms import FormTestimoni

import json


def about(request):
	data_obj = Testimonial.objects.all()
	if(request.method == 'POST'):
		testimonial_form = FormTestimoni(request.POST)
		if(testimonial_form.is_valid()):
			data = Testimonial(testimoni = request.POST['testimoni'],)
			print('datamasuk')
			data.save()
			return HttpResponseRedirect('about/')
	else:
		testimonial_form = FormTestimoni()
		return render(request, 'about.html', {'testimonial_form':testimonial_form,'data_obj':data_obj})
	
@csrf_exempt
def create_post(request):
	user = request.user
	if request.method == 'POST':
		post_text = request.POST.get('the_post')
		response_data = {}
		
		post = Testimonial(testimoni=post_text, user_name=user.username)
		post.save()
		
		try:
			response_data['result'] = 'Create post successful!'
			response_data['message'] = post_text
		except:
			response_data['result'] = 'Post is failed!'
			response_data['message'] = "whoops"
		return HttpResponse(json.dumps(response_data), content_type="application/json" )
	else:
		return HttpResponse(json.dumps({"nothing to see": "AAAAAAAAAAAAAAAarggg"}), content_type="application/json")

