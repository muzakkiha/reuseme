$(document).ready(function(){
    $('#post-form').on('submit', function(event){
        event.preventDefault();
        console.log("form submitted!")  // sanity check
        create_post();
    });



    function create_post() {
    console.log("create post is working!") // sanity check
    $.ajax({
        url 	: 'post', // the endpoint
        type 	: 'POST', // http method
        data 	: { the_post : $('#post-text').val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    
   }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            $('#post-text').val(''); // remove the value from the input
            //console.log(json); // log the returned json to the console
            $(".container").append(
                            '<div class="box-testi-part col-lg-8">'+
                            '<div class="text">'+
                            '<span>" '+json.message+' "</span></div></div>'
                            );
            console.log("success"); // another sanity check
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            console.log("fail");
        }
    });
};
})
