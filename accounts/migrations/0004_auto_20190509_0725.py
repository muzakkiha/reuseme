# Generated by Django 2.1.1 on 2019-05-09 00:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('listbarang', '0008_auto_20190509_0646'),
        ('accounts', '0003_auto_20190509_0716'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='barang_dibeli',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='dibeli', to='listbarang.Barang', unique=True),
        ),
        migrations.AddField(
            model_name='member',
            name='barang_dijual',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='dijual', to='listbarang.Barang', unique=True),
        ),
    ]
