from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import tracker
from member.models import Pengguna
from listbarang.models import Barang
from .models import BarangMember
# Create your tests here.


class TestTrackerApp(TestCase):
    def test_tracker_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_using_tracker_func(self):
        found = resolve('/tracker/')
        self.assertEqual(found.func, tracker)

    def test_tracker_template_contains(self):
        request = HttpRequest()
        response = tracker(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Member', html_response)

    def test_member_have_barang_dijual(self):
        member1 = Pengguna.objects.create(nomor="1234", nama="saul", username="saulandree",
                                          email="a@a.com", password='1a2a3a4a', alamat="manakek", bank="mandiin", norek="12121212")
        barang1 = Barang.objects.create(
            username="saulandreee", email="a@a.com", kategori="Bapak", nama_barang="Bapak", foto_barang="'https://carisinyal.com/wp-content/uploads/2018/04/Acer-Predator-21X.jpg'", deskripsi_barang="bapak bapak takut istri", harga_barang="20000")
        barangmember1 = BarangMember.objects.create(
            member=member1, barang=barang1)
        count = BarangMember.objects.all().count()
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertEqual(count, 1)
        self.assertIn("saulandreee", html_response)
        self.assertIn("Bapak", html_response)
