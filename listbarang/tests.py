from django.test import TestCase, Client
from django.urls import resolve

from .views import listbarang
from .models import Barang

class ListbarangUnitTest(TestCase):

    def test_page_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_listbarang_function(self):
        found = resolve('/')
        self.assertEqual(found.func, listbarang)

    def test_template_is_exist(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'listbarang.html')