from django.urls import path
from .views import listbarang, beli

urlpatterns = [
	path('', listbarang, name='listbarang'),
	path('listbarang/beli/<int:id_barang>', beli, name='beli'),
]