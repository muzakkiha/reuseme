from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .models import Barang
from accounts.models import Member

response = {}


def listbarang(request):
    title = 'Reuse me, please'
    response = {'title': title}

    response['barang_list'] = Barang.objects.all()
    return render(request, 'listbarang.html', response)

@csrf_exempt
def beli(request, id_barang):
    user = request.user 
    if (request.method == "POST"):
        barang = Barang.objects.filter(id=id_barang)
        barang.update(pembeli=user)
        return HttpResponseRedirect("/")